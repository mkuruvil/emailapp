module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['src/js/vendor/jquery-2.2.3.min.js','src/js/vendor/bootstrap.min.js', 'src/js/vendor/angular.min.js', 'src/js/vendor/ui-bootstrap-tpls.min.js', 'src/js/vendor/angular-ui-tree.min.js', 'src/js/vendor/ui-grid.min.js', 'src/js/emailapp.js','src/js/body.js','src/js/messagebody.js'],
                dest: 'dist/js/<%= pkg.name %>.js'
            }
        },
        copy: {
            dist: {
		      files: [
                {
                    expand: true,
                    src: 'src/css/**/*css',
                    dest: 'dist/css/',
		            flatten: true
                },
                {
                    expand: true,
                    src: 'src/*png',
                    dest: 'dist/',
                    flatten: true    
                },
                {
                    expand: true, 
                    cwd: 'src',
                    src: ['**/fonts/**'],
                    dest: 'dist/'
                },
               {
                    expand: true, 
                    cwd: 'src',
                    src: ['**/fonts/**'],
                    dest: 'dist/css'
                }                                               
              ]
            },
	        dev: {
              files: [
                {
                    expand: true,
                    src: ['js/**', 'css/**'],
                    dest: 'dist/css/',
                    flatten: true
                },
                {
                    expand: true,
                    src: 'src/*png',
                    dest: 'dist/',
                    flatten: true    
                }           
              ]
	       }
        },
        targethtml: {
            dist: {
                files: {
                    'dist/index.html': 'src/emailappmain.html',
                    'dist/header.htm': 'src/header.htm',                    
                    'dist/body.htm': 'src/body.htm',
                    'dist/messagebody.htm': 'src/messagebody.htm',
                    'dist/navigation.htm': 'src/navigation.htm',                    
                }
            },
            dev: {
                files: {
                    'dist/index.html': 'src/emailappmain.html',
                    'dist/body.htm': 'src/body.htm',
                    'dist/messagebody.htm': 'src/messagebody.htm'
                }
	        }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-targethtml');
    grunt.registerTask('dist', ['concat', 'targethtml:dist', 'copy:dist']);
    grunt.registerTask('dev', ['targethtml:dev', 'copy:dev']);
};
