/**m
 * 
 */
var app=angular.module('emailapp');
app.controller('NavigationController', ['$scope', function($scope) {
   $scope.data = [{ "title": "Adam Smith", "mailBoxes": [
                { "title": "Inbox", "messages": ["Inbox message 1", "Inbox message 2"] }, 
                { "title": "Sent Mail", "messages": ["Sent message 1", "Sent message 2"]}]}];
	$scope.toggle = function (scope) {
        scope.toggle();
    };
}
]);
