/**
 * 
 */
var app=angular.module('emailapp',['ui.tree', 'ui.bootstrap', 'ui.grid', 'ui.grid.resizeColumns']);
app.controller('Tests', function($scope) {
	  $scope.greeting = 'Hola!';

	$scope.gridOptionsSimple = {
        data: [
          {
              "name": "Lelia Gates",
              "gender": "female",
              "company": "Proxsoft"
          },
          {
              "name": "Letitia Vasquez",
              "gender": "female",
              "company": "Slumberia"
          },
          {
              "name": "Trevino Moreno",
              "gender": "male",
              "company": "Conjurica"
          }
        ]
      };

});